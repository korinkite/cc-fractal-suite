/*
	Cole Clark's Fractal Suite

	register.h
	Header defining code that adds nodes from the dso to Houdini.
 */

#pragma once

#include <OP/OP_OperatorTable.h>

void newCop2Operator(OP_OperatorTable* Table);
